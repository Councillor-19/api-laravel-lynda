<?php

use Behat\Behat\Context\Context;
use Behat\Gherkin\Node\PyStringNode;
use Behat\Gherkin\Node\TableNode;

/**
 * Defines application features from the specific context.
 */
class FeatureContext implements Context
{
	/**
	 * Initializes context.
	 *
	 * Every scenario gets its own context instance.
	 * You can also pass arbitrary arguments to the
	 * context constructor through behat.yml.
	 */
	public function __construct()
	{
		$this->bearerToken = "";
	}

	/**
	 * @Given I have the payload:
	 */
	public function iHaveThePayload(PyStringNode $string)
	{
		$this->payload = $string;
	}

	/**
	 * @When /^I request "(GET|PUT|POST|DELETE|PATCH) ([^"]*)"$/
	 */
	public function iRequest($httpMethod, $argument1)
	{
		$client = new GuzzleHttp\Client();
		$this->response = $client->request(
			$httpMethod,
			'http://laravelapi.loc' . $argument1,
			[
				'body' => $this->payload,
				'headers' => [
					"Authorization" => "Bearer {$this->bearerToken}",
					"Content-Type" => "application/json",
				],
			]
		);
		$this->responseBody = $this->response->getBody(true);
	}

	/**
	 * @Then /^I get a response$/
	 */
	public function iGetAResponse()
	{
		if (empty($this->responseBody)) {
			throw new Exception('Did not get a response from the API');
		}
	}

	/**
	 * @Given /^the response is JSON$/
	 */
	public function theResponseIsJson()
	{
		$data = json_decode($this->responseBody);

		if (empty($data)) {
			throw new Exception("Response was not JSON\n" . $this->responseBody);
		}
	}


	/**
	 * @Then the response contains :arg1 records
	 */
	public function theResponseContainsRecords($arg1)
	{
		$data = json_decode($this->responseBody);
		$count = count($data);
		return ($count == $arg1);
		//throw new PendingException();
	}

	/**
	 * @Then the question contains a title if :arg1
	 */
	public function theQuestionContainsATitleIf($arg1)
	{
		$data = json_decode($this->responseBody);

		if($data->title == $arg1) {

		} else {
			throw new Exception("This is not a question");
		}
	}
}